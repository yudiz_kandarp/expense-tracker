import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './Expenses.css'

class Expenses extends Component {
	constructor(props) {
		super(props)
		this.state = {
			tab: 'all',
		}
	}
	activeTab = (target) => {
		return this.state.tab === target ? 'active' : ''
	}
	changeTab = (target) => {
		this.setState({ tab: target })
	}

	render() {
		const { catagory, items, removeExpense, catagoryExpense } = this.props
		return (
			<div>
				<div className='nav'>
					<div
						onClick={() => this.changeTab('all')}
						className={`catagory ${this.activeTab('all')}`}
					>
						all
					</div>
					{catagory.map((cat, index) => {
						return (
							<div
								key={index}
								onClick={() => this.changeTab(cat)}
								className={`catagory ${this.activeTab(cat)}`}
							>
								{cat}
							</div>
						)
					})}
				</div>
				<SingleItem
					item={{ items, removeExpense, catagoryExpense }}
					target={this.state.tab}
				/>
			</div>
		)
	}
}
Expenses.propTypes = {
	catagory: PropTypes.array,
	items: PropTypes.array,
	catagoryExpense: PropTypes.func,
	TotalExpense: PropTypes.func,
	removeExpense: PropTypes.func,
}

class SingleItem extends Component {
	constructor(props) {
		super(props)
		this.state = {
			toggle: true,
		}
	}
	render() {
		const { item, target } = this.props
		const { removeExpense, catagoryExpense } = item
		let filtered = item.items.filter((item) => {
			return item.catagory == target
		})
		if (target === 'all') {
			filtered = item.items
		}

		return (
			<div>
				{filtered?.length > 0 ? (
					<div className='catagory'>
						your {target} expense is ₹ {catagoryExpense(target)}
					</div>
				) : (
					<div className='catagory'>no expenses yet</div>
				)}

				<div className='catagory_container'>
					{filtered?.map((item) => (
						<div className='catagory_item' key={item.id}>
							<h5 className='catagory_name'>{item.catagory}</h5>
							<h3 className='catagory_title'>{item.title}</h3>
							<div className='item_div'>
								<p className='item_price'>₹ {item.price} </p>
								<div className='remove' onClick={() => removeExpense(item.id)}>
									X
								</div>
							</div>
						</div>
					))}
				</div>
			</div>
		)
	}
}
SingleItem.propTypes = {
	item: PropTypes.shape({
		id: PropTypes.string,
		price: PropTypes.number,
		catagory: PropTypes.string,
		title: PropTypes.string,
		items: PropTypes.array,
		removeExpense: PropTypes.func,
		catagoryExpense: PropTypes.func,
	}),
	target: PropTypes.string,
}
export default Expenses
