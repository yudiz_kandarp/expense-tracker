import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './TakeData.css'
class TakeData extends Component {
	constructor(props) {
		super(props)
		this.state = {
			title: '',
			price: 0,
			catagory: 'all',
			error: '',
		}
	}
	handleChange = (e) => {
		this.setState({ [e.target.name]: e.target.value })
	}
	handleSubmit = (e) => {
		e.preventDefault()
		const { title, price, catagory } = this.state
		if (price <= 0) {
			this.setState({ error: 'enter positive expense' })
			return
		}
		if (title.trim().length === 0) {
			this.setState({ error: 'add title' })
			return
		}
		this.props.addExpense({ price, title, catagory })
		this.setState({ price: 0, title: '' })
		this.setState({ error: '' })
	}

	render() {
		return (
			<div className='addExpense_container'>
				<form onSubmit={this.handleSubmit} className='addExpense_form'>
					<select
						className='catagory'
						name='catagory'
						value={this.state.catagory}
						onChange={this.handleChange}
						autoFocus={true}
					>
						<option value='all'>all expenses</option>
						{this.props.catagory.map((item, index) => (
							<option key={index} value={item}>
								{item}
							</option>
						))}
					</select>
					<input
						className='addExpense_input'
						type='text'
						name='title'
						value={this.state.title}
						placeholder='add Title'
						onChange={this.handleChange}
						required
					/>
					<input
						className='addExpense_input'
						type='number'
						name='price'
						value={this.state.price}
						placeholder='add Amount here...'
						onChange={this.handleChange}
						required
					/>
					{this.state.error && (
						<div style={{ margin: '10px' }}>{this.state.error}</div>
					)}
					<input type='submit' value='add' className='addExpense_btn' />
				</form>
				<div className='total_expense'>
					<div>total Expense</div>
					<h3>{this.props.TotalExpense()}</h3>
				</div>
			</div>
		)
	}
}
TakeData.propTypes = {
	addExpense: PropTypes.func,
	TotalExpense: PropTypes.func,
	catagory: PropTypes.array,
}
export default TakeData
