import React, { Component } from 'react'
import { v4 as uuidv4 } from 'uuid'
import Expenses from './components/expenses/Expenses'
import TakeData from './components/form/TakeData'
import './components/expenses/Expenses.css'

class App extends Component {
	constructor(props) {
		super(props)
		this.state = {
			totalExpense: 0,
			catagory: ['food', 'home', 'travel', 'business', 'studies', 'medical'],
			items: [],
		}
	}

	addExpense = (data) => {
		this.setState((prev) => ({
			items: [
				...prev.items,
				{
					title: data.title,
					price: data.price,
					catagory: data.catagory,
					id: uuidv4(),
				},
			],
		}))
	}
	removeExpense = (id) => {
		const removedItems = this.state.items.filter((item) => item.id !== id)
		this.setState({
			items: [...removedItems],
		})
	}
	TotalExpense = () => {
		return this.state.items.reduce((acc, curr) => {
			return (acc += Number(curr.price))
		}, 0)
	}
	catagoryExpense = (value) => {
		if (value === 'all') {
			return this.TotalExpense()
		}
		return this.state.items
			.filter((item) => item.catagory == value)
			.reduce((acc, curr) => {
				return (acc += Number(curr.price))
			}, 0)
	}

	render() {
		return (
			<div>
				<TakeData
					catagory={this.state.catagory}
					addExpense={this.addExpense}
					TotalExpense={this.TotalExpense}
				/>
				<Expenses
					{...this.state}
					removeExpense={this.removeExpense}
					catagoryExpense={this.catagoryExpense}
					TotalExpense={this.TotalExpense}
				/>
			</div>
		)
	}
}

export default App
